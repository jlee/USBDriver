; Copyright 2015 Castle Technology Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;
        SUBT Exported USBDriver constants

OldOpt  SETA    {OPT}
        OPT     OptNoList+OptNoP1List

SWIClass        SETS    USBDriverSWI_Name

        ^       USBDriverSWI_Base

        AddSWI  USBDriver_RegisterBus
        AddSWI  USBDriver_DeregisterBus
        AddSWI  USBDriver_InsertTransfer
        AddSWI  USBDriver_TransferComplete
        AddSWI  USBDriver_ScheduleSoftInterrupt
        AddSWI  USBDriver_Version

; The structures in the 4 exported headers
;    NetBSD/dev/usb/h/usb     NetBSD/dev/usb/h/usbdivar
;    NetBSD/dev/usb/h/usbdi   NetBSD/dev/usb/h/usb_port
; are used by the USB host controller drivers to communicate with the main
; USBDriver module. Thus, we need to make sure that they only register
; with USBDriver if both modules use compatible structure definitions.
; So, whenever you make a backwards-incompatible change to these shared
; structures, make sure you update this API version to the version of the
; USBDriver module that first implemented those changes (not the dummy value
; in VersionNum file!).
RISCOS_USBDRIVER_API_VERSION * 80

        OPT     OldOpt
        END
